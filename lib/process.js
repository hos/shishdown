const Promise = require('bluebird')
const downloader = require('./downloader')
const uploader = require('./uploader')
const fs = Promise.promisifyAll(require('fs-extra'))
const sendEmail = require('./mail')
const settings = require('../settings/settings')
const path = require('path')
const uuid = require('uuid')

module.exports = ({page, email}) => {
  return Promise.coroutine(function*() {
    const pageName = (page.name || page.id).toString().replace(/"/g, '').replace(/\//g, '-').replace(/ /g, '-')
    const filePath = yield downloader(page)
    const remoteFilePath = path.join(settings.aws.public, uuid.v4(), pageName + '.zip')
    const remoteUrl = yield uploader(filePath, remoteFilePath)
    fs.removeAsync(filePath).catch(console.error)
    return sendEmail({
      email: email,
      subject: 'Image download success',
      url: remoteUrl
    })
  })().catch(err => {
    console.error(err)
    sendEmail({
      email: 'hovosanoyan@gmail.com',
      subject: 'Image download error',
      url: err.message || err
    })
  })
}
