const path = require('path')

module.exports = (() => {
  return {
    email: process.env.SHISH_EMAIL || 'support@shishdown.tk',
    projectsFolder: path.resolve(process.cwd(), 'projects'),
    port: process.env.PORT || 3000,
    postmark: {
      postmarkKey: process.env.POSTMARK_API_KEY
    },
    aws: {
      public: 'public',
      endpoint: 's3.eu-west-2.amazonaws.com',
      signatureVersion: 'v4',
      bucket: process.env.SHISH_AWS_BUCKET,
      accessKeyId: process.env.SHISH_AWS_ID,
      secretAccessKey: process.env.SHISH_AWS_SECRET
    },
    facebook: {
      appId: process.env.SHISH_FB_APP_ID,
      appSecret: process.env.SHISH_FB_APP_SECRET,
      accessToken: process.env.SHISH_FB_APP_TOKEN,
      version: 'v2.8',
      Promise: require('bluebird')
    }
  }
})()
