const settings = require('../settings/settings')
const {Facebook} = require('fb')
const fb = new Facebook(settings.facebook)
const archiver = require('archiver')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs-extra'))
const request = require('request')
const path = require('path')

const mkdir = dir => fs.ensureDirAsync(dir).catch(() => fs.mkdir(dir))

const archive = projectPath => new Promise((resolve, reject) => {
  const zipPath = fs.createWriteStream(projectPath + '.zip')
  const archive = archiver('zip', { store: true })
  zipPath.on('close', () => resolve(projectPath + '.zip'))
  zipPath.on('error', err => reject(err))
  archive.on('error', err => reject(err))
  archive.pipe(zipPath)
  archive.directory(projectPath, './')
  archive.finalize()
})

const getPage = (page) => new Promise((resolve, reject) => {
  fb.api(page + '?fields=albums{photos{images},name},name', function (res) {
    if (!res || res.error) return reject(res)
    return resolve(res)
  })
})

const largeSide = image => image.height > image.width ? 'height' : 'width'
const bestImage = images => {
  const side = largeSide(images[0])
  return images.sort((a, b) => b[side] - a[side])[0]
}

const downloadImage = (dir, photo) => new Promise((resolve, reject) => {
  const images = photo.images
  const image = bestImage(images)
  const destFile = path.join(dir, photo.name || photo.id) + '.jpg'
  const file = fs.createWriteStream(destFile)
  request.get(image.source)
  .on('error', reject)
  .on('end', () => resolve(destFile))
  .pipe(file)
})

const downloadAlbum = (dir, album) => {
  const albumPath = (album.name || album.id).toString()
    .replace(new RegExp('"', 'g'), '')
    .replace(/\//g, '-')

  const albumDir = path.join(dir, albumPath)
  const photos = album.photos

  return mkdir(albumDir).then(() => Promise.map(photos.data, photo => downloadImage(albumDir, photo)))
}

const downloadAlbums = (dir, albums) => {
  return Promise.map(albums, album => downloadAlbum(dir, album))
}

const download = page => {
  return Promise.coroutine(function*() {
    const pagePath = (page.name || page.id).toString()
        .replace(/"/g, '')
        .replace(/\//g, '-')
    const pageDir = path.join(settings.projectsFolder, pagePath)

    yield mkdir(pageDir)
    yield downloadAlbums(pageDir, page.albums.data)

    const zipFile = yield archive(pageDir).catch(console.error)
    fs.removeAsync(pageDir).catch(console.error)
    return zipFile
  })()
}

download.getPage = getPage
module.exports = download
