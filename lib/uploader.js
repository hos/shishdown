const settings = require('../settings/settings')
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const AWS = require('aws-sdk')
const uuid = require('uuid')

AWS.config.update({
  endpoint: settings.aws.endpoint,
  signatureVersion: settings.aws.signatureVersion,
  accessKeyId: settings.aws.accessKeyId,
  secretAccessKey: settings.aws.secretAccessKey
})

function uploadFileToS3GetLocation (srcFilePath, destFilePath, Bucket = settings.aws.bucket) {
  return uploadFileToS3(srcFilePath, destFilePath, Bucket)
    .then(result => result.Location.replace(/%2F/g, '/'))
}

function uploadFileToS3 (srcFilePath, destFilePath, Bucket) {
  return fs.readFileAsync(srcFilePath).then(function (fileStream) {
    const s3bucket = new AWS.S3({params: {Bucket}})
    const params = {
      Key: destFilePath,
      Body: fileStream
    }
    return s3Upload(s3bucket, params)
  })
}

function s3Upload (s3bucket, params) {
  return new Promise((resolve, reject) => {
    s3bucket.upload(params, (err, data) => {
      if (err) {
        return reject(err)
      }
      return resolve(data)
    })
  })
}

module.exports = uploadFileToS3GetLocation

