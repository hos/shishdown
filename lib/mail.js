const settings = require('../settings/settings')
const postmark = require('postmark')
const client = new postmark.Client(settings.postmark.postmarkKey)

const sendEmail = ({subject, url, email}) => new Promise((resolve, reject) => {
  client.sendEmail({
    'From': settings.email,
    'To': email,
    'Subject': subject,
    'TextBody': url
  }, (err, result) => {
    if (err) return reject(err)
    resolve(result)
  })
})

module.exports = sendEmail
