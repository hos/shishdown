var express = require('express')
var router = express.Router()
const process = require('../lib/process')
const downloader = require('../lib/downloader')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Shishdown' })
})

router.post('/', (req, res) => {
  const pageId = req.body.id
  const email = req.body.email

  console.log(`Request with Page ID : ${pageId} | Email : ${email}`)
  if (!pageId && !email) {
    return res.status(301).send(`please provide facebook page id and your email`)
  }
  if (!pageId) {
    return res.status(301).send(`please provide facebook page id or name`)
  }
  if (!email) {
    return res.status(301).send(`please provide your email`)
  }
  downloader.getPage(pageId).then(page => {
    res.send('Process started details will be emailed')
    process({page: page, email})
  }).catch(error => {
    console.error(error)
    res.status(301).send('Internal Server Error')
  })
})

module.exports = router
