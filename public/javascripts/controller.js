/* global angular */

var app = angular.module('ngApp', [])

app.controller('MainController', MainController)

MainController.$inject = ['$scope', '$http']
function MainController ($scope, $http) {
  $scope.getAlbum = function () {
    $http.post('/', {
      id: $scope.id,
      email: $scope.email
    }).then(function () {
      success()
      showMessage('DOWNLOAD WILL BE STARTED, WE WILL SEND YOU EMAIL.', 5000)
    }, function (res) {
      showMessage(res.data || "WE CAN'T PROCESS YOUR REQUEST", 5000)
      error()
    })
  }
}

var body = document.getElementsByTagName('body')[0]
function removeColor (t) {
  setTimeout(function () {
    body.style.animationName = ''
  }, t || 5000)
}
function error () {
  body.style.animationName = 'error'
  removeColor()
}
function success () {
  body.style.animationName = 'success'
  removeColor()
}
function showMessage (message, removeIn) {
  document.getElementById('message').style.opacity = 1
  document.getElementById('message').innerText = message
  removeIn && setTimeout(clearMessage, removeIn)
}
function clearMessage () {
  document.getElementById('message').style.opacity = 0
}
